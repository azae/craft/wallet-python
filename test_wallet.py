import pytest

from wallet import Amount, IsoCode, RateProvider, Wallet, Currency, UnknownCurrency, Stock, StockType, UnknownStockType


class TestWallet:
    def test_should_return_0_for_empty_wallet(self):
        expected = Amount(euro(), 0)
        assert expected == Wallet().value(euro(), ConstantRate())

    def test_should_return_value_of_stock_for_wallet_with_one_currency(self):
        euro_stock = Stock.from_currency(euro(), 10)
        expected = Amount(euro(), 10)
        assert expected == Wallet().add(euro_stock).value(euro(), ConstantRate())

    def test_should_return_value_of_stock_for_wallet_with_two_currency(self):
        expected = Amount(euro(), 5 + 3 * 2)
        assert expected == sample_wallet().value(euro(), ConstantRate())


def sample_wallet():
    euro_stock = Stock.from_currency(euro(), 5)
    brent_stock = Stock(StockType.BRENT, 3)
    wallet = Wallet().add(euro_stock).add(brent_stock)
    return wallet


def euro():
    return Currency.from_iso_code(IsoCode.EUR)


def dollar():
    return Currency.from_iso_code(IsoCode.USD)


class TestStockType:
    def test_euro(self):
        assert StockType.from_iso_code(IsoCode.EUR) == StockType.EURO

    def test_usd(self):
        assert StockType.from_iso_code(IsoCode.USD) == StockType.USD

    def test_fails_when_unknown_stock_type(self):
        with pytest.raises(UnknownStockType):
            StockType.from_iso_code('UNKNOWN')


class TestCurrency:
    def test_equals_when_same_iso_code(self):
        assert dollar() == dollar()

    def test_not_be_equals_otherwise(self):
        assert dollar() != euro()

    def test_euro(self):
        assert Currency.from_iso_code(IsoCode.EUR).symbol == "€"
        assert Currency.from_iso_code(IsoCode.EUR).precision == 2

    def test_usd(self):
        assert Currency.from_iso_code(IsoCode.USD).symbol == "$"
        assert Currency.from_iso_code(IsoCode.USD).precision == 3

    def test_fails_when_unknown_iso_code(self):
        with pytest.raises(UnknownCurrency):
            Currency.from_iso_code('UNKNOWN')


class TestAmount:
    def test_equals_when_same_quantity_and_currency(self):
        assert Amount(euro(), 1) == Amount(euro(), 1)

    def test_not_equals_when_different_quantity(self):
        assert Amount(euro(), 1) != Amount(euro(), 2)

    def test_not_equals_when_different_currency(self):
        assert Amount(euro(), 1) != Amount(dollar(), 1)

    def test_quantity_rounded_with_currency_precision(self):
        assert 1.23 == Amount(euro(), 1.234).quantity
        assert 1.234 == Amount(dollar(), 1.2345).quantity

    def test_quantity_rounded_up_when_above_or_equal_5(self):
        assert 1.24 == Amount(euro(), 1.235).quantity

    def test_rounded_amount(self):
        assert Amount(euro(), 1.23) == Amount(euro(), 1.231)
        assert Amount(dollar(), 1.23) != Amount(dollar(), 1.231)


class ConstantRate(RateProvider):
    def rate(self, f: StockType, t: Currency):
        if f == StockType.EURO and t.iso_code == IsoCode.EUR:
            return 1
        return 2
